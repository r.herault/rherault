# rherault :tada:

## Commencer

### Installer les dépendances

```bash
composer install
```

```bash
yarn install
```

### Compiler les assets

```bash
yarn dev
```

### Base de données

> Avant de créer la base de données, il faut créer un fichier `.env.local` et copier la ligne DATABASE_URL du fichier .env dedans en la modifiant avec vos informations.

Créer la base de données :

```bash
php bin/console doctrine:database:create
```

Appliquer les migrations :

```bash
php bin/console doctrine:migrations:migrate
```

### Démarrer l'application

> Pour démarrer l'application, il faut installer `symfony`, vous pouvez l'installer via :
> `wget https://get.symfony.com/cli/installer -O - | bash`

```bash
symfony serve -d --allow-http
```

Le site est disponible à l'adresse [localhost:8000](https://localhost:8000).

## Développement

### Créer un utilisateur

Vous pouvez créer un utilisateur avec la commande suivante :

```bash
php bin/console app:add-user
```

> Ajouter `--admin` à la fin de cette commande pour créer un administrateur

### Lancer un serveur de mail

Un serveur de mail est disponible en lançant cette commande :

```bash
npx maildev --hide-extensions STARTTLS
```

Vous pouvez à présent recevoir les mails de développement à l'adresse [localhost:1080](http://localhost:1080)


### Pour lancer les tests

```bash
php bin/phpunit
```

> Il se peut qu'au premier lancement les tests échoues car la base de données de test n'est pas encore créée, relancer la commande.

## Développeurs
* **Romain HERAULT** - [r.herault](https://rherault.fr)
