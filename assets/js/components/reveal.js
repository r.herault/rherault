const ratio = 0.1;

const options = {
    root: null,
    rootMargin: '0px',
    threshold: ratio,
};

const callback = (entries, observer) => {
    entries.forEach((entry) => {
        if (entry.intersectionRatio > ratio) {
            entry.target.classList.remove('reveal');
            observer.unobserve(entry.target);
        }
    });
};

document.documentElement.classList.add('reveal-loaded');
window.addEventListener('DOMContentLoaded', () => {
    const observer = new IntersectionObserver(callback, options);
    document.querySelectorAll('.reveal').forEach((r) => {
        observer.observe(r);
    });
});