import axios from 'axios';

export function fetchVersion() {
	return axios.get('/rh-admin/api/version');
}

export function fetchInfos() {
	return axios.get('/rh-admin/api/infos');
}

export function fetchServer() {
	return axios.get('/rh-admin/api/server');
}

export function fetchVisitors() {
	return axios.get('/rh-admin/api/visitors');
}
