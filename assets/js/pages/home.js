import 'styles/pages/_homepage.scss';

import Vue from 'vue';

import VueCarousel from 'vue-carousel';

// import Rellax from 'rellax';
import lax from 'lax.js';

import { TweenMax, TimelineMax, Quad } from 'gsap/all';
import ScrollMagic from 'scrollmagic';
import Card from '../../vue/components/Card';
import Progress from '../../vue/components/Progress';
import '@/vendor/plugins/animation.gsap';

import '@/vendor/fittext';

Vue.use(VueCarousel);

new Vue({
    el: '#services',
    components: {
        progressbar: Progress,
        card: Card,
    },
});

window.onload = function () {
    lax.setup({
        breakpoints: { small: 0, large: 767 },
    });

    const updateLax = () => {
        lax.update(window.scrollY);
        window.requestAnimationFrame(updateLax);
    };

    window.requestAnimationFrame(updateLax);
};

document.getElementsByClassName('projects-parallax').forEach((item) => {
    item.setAttribute('data-lax-translate-y', '600 -200, -2800 700');
    item.setAttribute('data-lax-translate-y_small', '0 0');
    item.setAttribute('data-lax-anchor', 'self');
});

window.fitText(document.getElementById('heading-zoom-title'), 0.48);

// let rellax = new Rellax('.projects-parallax');

const title = new TimelineMax()
    .set('.heading-zoom-title', {
        scale: 1,
        transformOrigin: '50.4% 50%',
        ease: Quad.easeIn,
        force3D: false,
    })
    .to('.heading-zoom-title', 0.01, {
        scale: 1,
        x: '0%',
        ease: Quad.easeIn,
        force3D: false,
    })
    .to('.heading-zoom-title', 1, {
        scale: 100,
        x: '-100%',
        ease: Quad.easeIn,
        force3D: false,
    });

const controller = new ScrollMagic.Controller();

const scale_scene = new ScrollMagic.Scene({
    triggerElement: '.heading-zoom',
    triggerHook: 0,
    duration: '200%',
})
    .setTween(title)
    .setPin('.heading-zoom')
    .addTo(controller);
