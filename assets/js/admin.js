import Vue from 'vue';
import App from './admin/admin';

new Vue({
    render: (h) => h(App),
}).$mount('#app');
