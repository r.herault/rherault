<?php

namespace App\Tests\Repository;

use App\Repository\ProjectRepository;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Liip\TestFixturesBundle\Test\FixturesTrait;

class ProjectRepositoryTest extends KernelTestCase {

    use FixturesTrait;

    public function testCount() {
        self::bootKernel();

        $this->loadFixtureFiles([
            __DIR__ . '/ProjectRepositoryTestFixtures.yaml'
        ]);

        $projects = self::$container->get(ProjectRepository::class)->count([]);

        $this->assertEquals(10, $projects);
    }
}