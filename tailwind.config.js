module.exports = {
  theme: {
    extend: {
      colors: {
        primary: "#f95959",
        beige: "#e3e3e3"
      }
    },
    fontFamily: {
      display: ["Raleway", "sans-serif"],
      body: ["Roboto", "sans-serif"]
    },

    container: {
      center: true,
      padding: "1rem"
    }
  },
  variants: {},
  plugins: []
};
