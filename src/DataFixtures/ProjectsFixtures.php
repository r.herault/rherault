<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Project;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;

class ProjectsFixtures extends Fixture
{
	private $faker;

    public function __construct()
    {
        $this->faker = Faker\Factory::create('fr_FR');
    }

    public function load(ObjectManager $manager)
    {
   		$categories[] = (new Category())
            ->setName('Symfony')
        ;
        $categories[] = (new Category())
            ->setName('Sass')
        ;
        $categories[] = (new Category())
            ->setName('WordPress')
        ;
        $categories[] = (new Category())
            ->setName('Django')
        ;
        $categories[] = (new Category())
            ->setName('Flutter')
        ;

        foreach ($categories as $category) {
            $manager->persist($category);
        }

        for ($i = 0; $i < 20; ++$i) {
            $project = (new Project())
                ->setTitle($this->faker->company)
                ->setDescription($this->faker->paragraph($nbSentences = 3, $variableNbSentences = true))
                ->setUrl($this->faker->domainName)
                ->addCategory($this->faker->randomElement($categories))
            ;

            $manager->persist($project);
        }

        $manager->flush();

    }
}
