<?php

namespace App\DataFixtures;

use App\Entity\Post;
use App\Entity\Tag;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;

class PostsFixtures extends Fixture
{

 	private $faker;

    public function __construct()
    {
        $this->faker = Faker\Factory::create('fr_FR');
    }

    public function load(ObjectManager $manager)
    {
		$tags = [];

        $tags[] = (new Tag())
            ->setName('Symfony')
        ;
        $tags[] = (new Tag())
            ->setName('Linux')
        ;
        $tags[] = (new Tag())
            ->setName('DevOps')
        ;
        $tags[] = (new Tag())
            ->setName('Hébergement')
        ;
        $tags[] = (new Tag())
            ->setName('Tutos')
        ;
        $tags[] = (new Tag())
            ->setName('Memos')
        ;

        foreach ($tags as $tag) {
            $manager->persist($tag);
        }

        for ($i = 0; $i < 20; ++$i) {
            $project = (new Post())
                ->setTitle($this->faker->word)
                ->setSlug($this->faker->slug)
                ->setContent($this->faker->paragraph(20))
                ->setSummary($this->faker->text(120))
                ->addTag($this->faker->randomElement($tags))
            ;

            $manager->persist($project);
        }

        $manager->flush();

    }
}
