<?php


namespace App\Notification;

use App\Utils\SpamChecker;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Twig\Environment;
use Symfony\Component\Mailer\MailerInterface;

class ContactNotification
{
    private $mailer;
	private $renderer;
	private $spamChecker;

    public function __construct(MailerInterface $mailer, Environment $renderer, SpamChecker $spamChecker)
    {
        $this->mailer = $mailer;
		$this->renderer = $renderer;
		$this->spamChecker = $spamChecker;
    }

    public function notify($data, $context)
    {
		if(2 === $this->spamChecker->getSpamScore($data, $context)) {
			throw new \RuntimeException('Blatan spam, go away !');
		}

        $email = (new TemplatedEmail())
            ->from('noreply@rherault.fr')
            ->to('romain@rherault.fr')
            ->replyTo($data['email'])
            ->subject('[Contact] rherault.fr : '.$data['subject'])
            ->htmlTemplate('emails/contact.html.twig')
            ->context([
                'contact' => $data,
            ]);


        $this->mailer->send($email);
    }
}
