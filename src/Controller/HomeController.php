<?php

namespace App\Controller;

use App\Repository\ProjectRepository;
use App\Repository\ServiceRepository;
use App\Repository\SkillRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(
        ProjectRepository $projectRepository,
        ServiceRepository $serviceRepository,
        SkillRepository $skillRepository
    ) {
        return $this->render('pages/home.html.twig', [
            'projects' => $projectRepository->findLatest(),
            'services' => $serviceRepository->findBy(
                [],
                ['displayOrder' => 'ASC']
            ),
            'skills' => $skillRepository->findBy(
                [],
                ['displayOrder' => 'ASC']
            ),
        ]);
    }
}
