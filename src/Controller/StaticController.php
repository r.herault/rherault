<?php

namespace App\Controller;

use App\Form\ContactType;
use App\Notification\ContactNotification;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class StaticController extends AbstractController
{

    /**
     * @Route("/contact", name="contact")
     */
    public function contact(Request $request, ContactNotification $notification)
    {
        $form = $this->createForm(ContactType::class, null);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
			$context = [
                'user_ip' => $request->getClientIp(),
                'user_agent' => $request->headers->get('user-agent'),
                'referrer' => $request->headers->get('referer'),
                'permalink' => $request->getUri(),
			];

            $notification->notify($form->getData(), $context);

            $this->addFlash('success', 'Votre message a bien été envoyé !');

            return $this->redirectToRoute('contact');
        }

        return $this->render('pages/contact.html.twig', [
            'headerBlack' => true,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/a-propos", name="about")
     */
    public function about()
    {
        $tools = [
            ['img' => 'media/images/vscode.webp', 'name' => 'Visual Studio Code', 'description' => 'Éditeur'],
            ['img' => 'media/images/git.webp', 'name' => 'Git', 'description' => 'Versionnage'],
            ['img' => 'media/images/webpack.webp', 'name' => 'Webpack', 'description' => 'Bundler'],
            ['img' => 'media/images/firefox.webp', 'name' => 'Firefox', 'description' => 'Navigateur'],
            ['img' => 'media/images/linux.webp', 'name' => 'Linux', 'description' => 'OS'],
            ['img' => 'media/images/figma.webp', 'name' => 'Figma', 'description' => 'Maquette'],
        ];

        return $this->render('pages/about.html.twig', [
            'tools' => $tools,
        ]);
    }
}
