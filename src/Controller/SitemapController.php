<?php

namespace App\Controller;

use App\Entity\Project;
use App\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SitemapController extends AbstractController
{
    /**
     * @Route("/sitemap.xml", name="sitemap", defaults={"_format"="xml"})
     */
    public function sitemap(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $urls = [];
        $hostname = $request->getSchemeAndHttpHost();

        // static urls
        $urls[] = ['loc' => $this->generateUrl('home')];
        $urls[] = ['loc' => $this->generateUrl('about')];
        $urls[] = ['loc' => $this->generateUrl('projects_index')];
        $urls[] = ['loc' => $this->generateUrl('contact')];
        $urls[] = ['loc' => $this->generateUrl('blog_index')];

        foreach ($em->getRepository(Project::class)->findAll() as $project) {
            $urls[] = ['loc' => $this->generateUrl(
                'project_show',
                ['slug' => $project->getSlug()]
            ), ];
        }
        
        foreach ($em->getRepository(Post::class)->findBy(['published' => true]) as $post) {
            $urls[] = ['loc' => $this->generateUrl(
                'blog_post',
                ['slug' => $post->getSlug()]
            ), ];
        }

        $response =  new Response(
            $this->renderView(
                'sitemap/sitemap.html.twig',
                ['urls' => $urls, 'hostname' => $hostname]
            ),
            200
        );

        $response->headers->set('Content-Type', 'text/xml');

        return $response;
    }
}
