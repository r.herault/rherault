<?php

namespace App\Controller\Admin;

use App\Entity\Service;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ServicesCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Service::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setSearchFields(['id', 'title', 'description', 'displayOrder']);
    }

    public function configureFields(string $pageName): iterable
    {
        $title = TextField::new('title');
        $description = TextareaField::new('description');
        $displayOrder = IntegerField::new('displayOrder');
        $id = IntegerField::new('id', 'ID');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $title, $displayOrder];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$id, $title, $description, $displayOrder];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$title, $description, $displayOrder];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$title, $description, $displayOrder];
        }
    }
}
