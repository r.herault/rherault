<?php

namespace App\Controller\Admin;

use App\Entity\Post;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class PostsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Post::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setSearchFields(['id', 'title', 'slug', 'summary', 'content']);
    }

    public function configureFields(string $pageName): iterable
    {
        $title = TextField::new('title');
        $slug = TextField::new('slug');
        $summary = TextField::new('summary');
        $published = BooleanField::new('published');
        $content = TextEditorField::new('content');
        $tags = AssociationField::new('tags');
        $id = IntegerField::new('id', 'ID');
        $publishedAt = DateTimeField::new('publishedAt');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$published, $title, $slug, $publishedAt];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$id, $title, $slug, $summary, $content, $publishedAt, $published, $tags];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$title, $slug, $summary, $published, $content, $tags];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$title, $slug, $summary, $published, $content, $tags];
        }
    }
}
