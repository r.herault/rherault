<?php

namespace App\Controller\Admin;

use App\Entity\Project;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Vich\UploaderBundle\Form\Type\VichImageType;

class ProjectsCrudController extends AbstractCrudController
{

    public static function getEntityFqcn(): string
    {
        return Project::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setSearchFields(['id', 'title', 'description', 'url', 'image', 'slug']);
    }

    public function configureFields(string $pageName): iterable
    {
        $title = TextField::new('title');
        $description = TextEditorField::new('description');
        $highlight = BooleanField::new('highlight');
        $categories = AssociationField::new('categories');
        $url = TextField::new('url');
        $id = IntegerField::new('id', 'ID');
        $image = ImageField::new('image')
            ->setBasePath($this->getParameter('app.path.project_images'));
        $imageFile = ImageField::new('imageFile')
            ->setFormType(VichImageType::class);
        $updatedAt = DateTimeField::new('updatedAt');
        $slug = TextField::new('slug');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$highlight, $title, $url, $image];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$id, $title, $description, $url, $imageFile, $updatedAt, $highlight, $slug, $categories];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$title, $description, $highlight, $categories, $url, $imageFile];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$title, $description, $highlight, $categories, $url, $imageFile];
        }
    }
}
