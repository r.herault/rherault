<?php

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Entity\Post;
use App\Entity\Project;
use App\Entity\Service;
use App\Entity\Skill;
use App\Entity\Tag;
use App\Repository\PostRepository;
use App\Repository\ProjectRepository;
use App\Service\MatomoHelper;
use App\Service\ServerHelper;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/rh-admin")
 */
class DashboardController extends AbstractDashboardController
{

    /**
     * @Route("/", name="admin_dashboard")
     */
    public function index(): Response
    {
        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('rherault');
    }

    public function configureCrud(): Crud
    {
        return Crud::new()
            ->setDateTimeFormat('dd/MM/yyyy HH:mm');
    }
    
    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoRoute('Dashboard', 'fas fa-tachometer-alt', 'admin_dashboard');
        yield MenuItem::linktoRoute('Voir le site', 'fas fa-home', 'home');

        yield MenuItem::section('Manager', 'fas fa-folder-open');
        yield MenuItem::linkToCrud('Projects', 'fas fa-tasks', Project::class);
        yield MenuItem::linkToCrud('Categories', 'fas fa-tag', Category::class);
        yield MenuItem::linkToCrud('Services', 'fas fa-folder-open', Service::class);
        yield MenuItem::linkToCrud('Skills', 'fas fa-folder-open', Skill::class);

        yield MenuItem::section('Blog', 'fas fa-folder-open');
        yield MenuItem::linkToCrud('Posts', 'fas fa-coffee', Post::class);
        yield MenuItem::linkToCrud('Tags', 'fas fa-tag', Tag::class);
    }
    
    /**
     * @Route("/api/version", defaults={"_format": "json"})
     *
     * @return Response
     */
    public function getVersion()
    {
        return $this->json([
            'php' => [
                'title' => 'Version de PHP',
                'info' => phpversion(),
                'grid' => 6,
            ],
            'symfony' => [
                'title' => 'Version de Symfony',
                'info' => \Symfony\Component\HttpKernel\Kernel::VERSION,
                'grid' => 6,
            ],
        ]);
    }
    
    /**
     * @Route("/api/infos", defaults={"_format": "json"})
     *
     * @return Response
     */
    public function getInfos(ProjectRepository $projectRepository, PostRepository $postRepository)
    {
        return $this->json([
            'projects' => [
                'title' => 'Nombre de projets',
                'info' => $projectRepository->getNumber(),
                'grid' => 6,
            ],
            'posts' => [
                'title' => 'Nombre d\'articles',
                'info' => $postRepository->getNumber(),
                'grid' => 6,
            ],
        ]);
    }
    
    /**
     * @Route("/api/server", defaults={"_format": "json"})
     *
     * @return Response
     */
    public function getServerInfos(ServerHelper $serverHelper)
    {
        return $this->json([
            'ram' => [
                'title' => 'Usage RAM',
                'info' => $serverHelper->getUsedMem('gb').'Gb/'.$serverHelper->getTotalMem('gb').'Gb',
                'grid' => 6,
            ],
            'percentage' => [
                'title' => 'Usage RAM (%)',
                'info' => $serverHelper->getUsedMemPercentage().'%',
                'grid' => 6,
            ],
        ]);
    }
    
    /**
     * @Route("/api/visitors", defaults={"_format": "json"})
     *
     * @return Response
     */
    public function getVisitors(MatomoHelper $matomoHelper)
    {
        return $this->json([
            'ram' => [
                'title' => 'Visiteurs (1 semaines)',
                'info' => $matomoHelper->getUniqueVisitors(),
                'grid' => 6,
            ],
            'percentage' => [
                'title' => 'Temps moyen',
                'info' => $matomoHelper->getVisitsTime(),
                'grid' => 6,
            ],
        ]);
    }
}
