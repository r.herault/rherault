<?php

namespace App\Controller\Admin;

use App\Entity\Skill;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class SkillsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Skill::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setSearchFields(['id', 'name', 'percentage', 'displayOrder']);
    }

    public function configureFields(string $pageName): iterable
    {
        $name = TextField::new('name');
        $percentage = IntegerField::new('percentage');
        $displayOrder = IntegerField::new('displayOrder');
        $id = IntegerField::new('id', 'ID');

        if (Crud::PAGE_INDEX === $pageName) {
            return [$id, $name, $percentage, $displayOrder];
        } elseif (Crud::PAGE_DETAIL === $pageName) {
            return [$id, $name, $percentage, $displayOrder];
        } elseif (Crud::PAGE_NEW === $pageName) {
            return [$name, $percentage, $displayOrder];
        } elseif (Crud::PAGE_EDIT === $pageName) {
            return [$name, $percentage, $displayOrder];
        }
    }
}
