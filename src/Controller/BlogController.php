<?php

namespace App\Controller;

use App\Entity\Post;
use App\Repository\PostRepository;
use App\Repository\TagRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @Route("/blog")
 */
class BlogController extends AbstractController
{
    /**
     * @Route("/", defaults={"_format"="html"}, methods="GET", name="blog_index")
     * @Route("/rss.xml", defaults={"_format"="xml"}, methods="GET", name="blog_rss")
     *
     * @Cache(smaxage="10")
     *
     * @SuppressWarnings(PHPMD)
     */
    public function index(
        Request $request,
        string $_format,
        PostRepository $posts,
        PaginatorInterface $paginator
    ): Response {
        $postsPerPage = 10;

        $posts = $paginator->paginate(
            $posts->findBy(['published' => true], ['publishedAt' => 'DESC']),
            $request->query->getInt('page', 1),
            $postsPerPage
        );
        
        return $this->render('blog/index.'.$_format.'.twig', [
            'posts' => $posts,
        ]);
    }

    /**
     * @Route("/{slug}", methods="GET", name="blog_post")
     */
    public function postShow(Post $post): Response
    {
        return $this->render('blog/post_show.html.twig', ['post' => $post]);
    }
}
