<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Project;
use App\Repository\CategoryRepository;
use App\Repository\ProjectRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/projets")
 */
class ProjectController extends AbstractController
{
    /**
     * @Route("/", name="projects_index")
     */
    public function index(ProjectRepository $projectRepository, PaginatorInterface $paginator, Request $request)
    {
        $projects = $paginator->paginate(
            $projectRepository->findBy([], ['id' => 'DESC']),
            $request->query->getInt('page', 1),
            6
        );

        return $this->render('project/index.html.twig', [
            'projects' => $projects,
            'headerBlack' => true,
        ]);
    }

    /**
     * @Route("/{slug}", name="project_show", requirements={"slug": "[a-z0-9\-]*"})
     */
    public function show(Project $project)
    {
        return $this->render('project/show.html.twig', [
            'project' => $project,
            'headerBlack' => true,
        ]);
    }
    
    /**
     * @Route("/technologie/{category_name}", name="project_by_category")
     */
    public function projectByCategory(
        string $categoryName,
        CategoryRepository $categoryRepository,
        ProjectRepository $projectRepository,
        PaginatorInterface $paginator,
        Request $request
    ) {

        $category = $categoryRepository->findBy(['name' => $categoryName]);
        dump($category);

        if ($category) {
            $projects = $paginator->paginate(
                $projectRepository->findBy(
                    ['categories' => $category],
                    ['id' => 'DESC']
                ),
                $request->query->getInt('page', 1),
                6
            );
        } else {
            throw $this->createNotFoundException('Impossible de trouver cette catégorie.');
        }

        return $this->render('project/index.html.twig', [
            'projects' => $projects,
            'headerBlack' => true,
            'category' => $category,
        ]);
    }
}
