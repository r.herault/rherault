<?php

namespace App\Service;

class ServerHelper
{

    /**
     * Get total memory
     *
     * @param string $args
     *
     * @return float
     */
    public function getTotalMem($args)
    {
        $file = file('/proc/meminfo');
        $fileLine = [];

        $fileLine = $file;

        $memtotal = $fileLine[0];
        $memtotal = filter_var(
            $memtotal,
            FILTER_SANITIZE_NUMBER_FLOAT,
            FILTER_FLAG_ALLOW_FRACTION | FILTER_FLAG_ALLOW_THOUSAND
        );

        if ('kb' == $args) {
            return (int) $memtotal;
        } elseif ('mb' == $args) {
            $memTotalMb = $this->convert($memtotal, 'mb');

            return $memTotalMb;
        } elseif ('gb' == $args) {
            $memTotalGb = $this->convert($memtotal, 'gb');

            return $memTotalGb;
        }
    }

    /**
     * Get available memory
     *
     * @param string $args
     *
     * @return float
     */
    public function getAvailableMem($args)
    {
        $file = file('/proc/meminfo');
        $fileLine = [];

        $fileLine = $file;

        $memavailable = $fileLine[2];
        $memavailable = filter_var(
            $memavailable,
            FILTER_SANITIZE_NUMBER_FLOAT,
            FILTER_FLAG_ALLOW_FRACTION | FILTER_FLAG_ALLOW_THOUSAND
        );

        if ('kb' == $args) {
            return (int) $memavailable;
        } elseif ('mb' == $args) {
            $memAvailableMb = $this->convert($memavailable, 'mb');

            return $memAvailableMb;
        } elseif ('gb' == $args) {
            $memAvailableGb = $this->convert($memavailable, 'gb');

            return $memAvailableGb;
        }
    }

    /**
     * Get cached memory
     *
     * @param [type] $args
     *
     * @return void
     */
    public function getCachedMem($args)
    {
        $file = file('/proc/meminfo');
        $fileLine = [];

        $fileLine = $file;

        $memcached = $fileLine[4];
        $memcached = filter_var(
            $memcached,
            FILTER_SANITIZE_NUMBER_FLOAT,
            FILTER_FLAG_ALLOW_FRACTION | FILTER_FLAG_ALLOW_THOUSAND
        );

        if ('kb' == $args) {
            return (int) $memcached;
        } elseif ('mb' == $args) {
            $memCachedMb = $this->convert($memcached, 'mb');

            return $memCachedMb;
        } elseif ('gb' == $args) {
            $memCachedGb = $this->convert($memcached, 'gb');

            return $memCachedGb;
        }
    }

    /**
     * Get swap memory
     *
     * @param string $args
     *
     * @return float
     */
    public function getSwapMem($args)
    {
        $file = file('/proc/meminfo');
        $fileLine = [];

        $fileLine = $file;

        $memswap = $fileLine[14];
        $memswap = filter_var(
            $memswap,
            FILTER_SANITIZE_NUMBER_FLOAT,
            FILTER_FLAG_ALLOW_FRACTION | FILTER_FLAG_ALLOW_THOUSAND
        );

        if ('kb' == $args) {
            return (int) $memswap;
        } elseif ('mb' == $args) {
            $memSwapMb = $this->convert($memswap, 'mb');

            return $memSwapMb;
        } elseif ('gb' == $args) {
            $memSwapGb = $this->convert($memswap, 'gb');

            return $memSwapGb;
        }
    }

    /**
     * Get buffer memory
     *
     * @param string $args
     *
     * @return float
     */
    public function getBufferMem($args)
    {
        $file = file('/proc/meminfo');
        $fileLine = [];

        $fileLine = $file;

        $membuffer = $fileLine[3];
        $membuffer = filter_var(
            $membuffer,
            FILTER_SANITIZE_NUMBER_FLOAT,
            FILTER_FLAG_ALLOW_FRACTION | FILTER_FLAG_ALLOW_THOUSAND
        );

        if ('kb' == $args) {
            return (int) $membuffer;
        } elseif ('mb' == $args) {
            $memBufferMb = $this->convert($membuffer, 'mb');

            return $memBufferMb;
        } elseif ('gb' == $args) {
            $memBufferGb = $this->convert($membuffer, 'gb');

            return $memBufferGb;
        }
    }

    /**
     * Get Shmem memory
     *
     * @param string $args
     *
     * @return float
     */
    public function getShmemMem($args)
    {
        $file = file('/proc/meminfo');
        $fileLine = [];

        $fileLine = $file;

        $shmem = $fileLine[20];
        $shmem = filter_var(
            $shmem,
            FILTER_SANITIZE_NUMBER_FLOAT,
            FILTER_FLAG_ALLOW_FRACTION | FILTER_FLAG_ALLOW_THOUSAND
        );

        if ('kb' == $args) {
            return (int) $shmem;
        } elseif ('mb' == $args) {
            $shmemMb = $this->convert($shmem, 'mb');

            return $shmemMb;
        } elseif ('gb' == $args) {
            $shmemGb = $this->convert($shmem, 'gb');

            return $shmemGb;
        }
    }

    /**
     * Get SReclaimable memory
     *
     * @param string $args
     *
     * @return float
     */
    public function getSreclaimableMem($args)
    {
        $file = file('/proc/meminfo');
        $fileLine = [];

        $fileLine = $file;

        $sreclaimable = $fileLine[22];
        $sreclaimable = filter_var(
            $sreclaimable,
            FILTER_SANITIZE_NUMBER_FLOAT,
            FILTER_FLAG_ALLOW_FRACTION | FILTER_FLAG_ALLOW_THOUSAND
        );

        if ('kb' == $args) {
            return (int) $sreclaimable;
        } elseif ('mb' == $args) {
            $sreclaimableMb = $this->convert($sreclaimable, 'mb');

            return $sreclaimableMb;
        } elseif ('gb' == $args) {
            $sreclaimableGb = $this->convert($sreclaimable, 'gb');

            return $sreclaimableGb;
        }
    }

    /**
     * Get SUnreclaim memory
     *
     * @param string $args
     *
     * @return float
     */
    public function getSunreclaimMem($args)
    {
        $file = file('/proc/meminfo');
        $fileLine = [];

        $fileLine = $file;

        $sunreclaim = $fileLine[23];
        $sunreclaim = filter_var(
            $sunreclaim,
            FILTER_SANITIZE_NUMBER_FLOAT,
            FILTER_FLAG_ALLOW_FRACTION | FILTER_FLAG_ALLOW_THOUSAND
        );

        if ('kb' == $args) {
            return (int) $sunreclaim;
        } elseif ('mb' == $args) {
            $sunreclaimMb = $this->convert($sunreclaim, 'mb');

            return $sunreclaimMb;
        } elseif ('gb' == $args) {
            $sunreclaimGb = $this->convert($sunreclaim, 'gb');

            return $sunreclaimGb;
        }
    }

    /**
     * Get free memory
     *
     * @param string $args
     *
     * @return float
     */
    public function getFreeMem($args)
    {
        $file = file('/proc/meminfo');
        $fileLine = [];

        $fileLine = $file;

        $memfree = $fileLine[1];
        $memfree = filter_var(
            $memfree,
            FILTER_SANITIZE_NUMBER_FLOAT,
            FILTER_FLAG_ALLOW_FRACTION | FILTER_FLAG_ALLOW_THOUSAND
        );

        if ('kb' == $args) {
            return  (int) $memfree;
        } elseif ('mb' == $args) {
            $memfreeMb = $this->convert($memfree, 'mb');

            return $memfreeMb;
        } elseif ('gb' == $args) {
            $memfreeGb = $this->convert($memfree, 'gb');

            return $memfreeGb;
        }
    }

    /**
     * Get used memory
     *
     * @param string $args
     *
     * @return float
     */
    public function getUsedMem($args)
    {
        $totalmem = $this->getTotalMem('kb');
        $freemem = $this->getFreeMem('kb');
        $buffers = $this->getBufferMem('kb');
        $cachedmem = $this->getCachedMem('kb');
        $sreclaimable = $this->getSreclaimableMem('kb');
        $shmem = $this->getShmemMem('kb');

        $usedmem = $totalmem - $freemem - $buffers - $cachedmem - $sreclaimable + $shmem;

        if ('kb' == $args) {
            return (int) $usedmem;
        } elseif ('mb' == $args) {
            $usedMemMb = $this->convert($usedmem, 'mb');

            return $usedMemMb;
        } elseif ('gb' == $args) {
            $usedMemGb = $this->convert($usedmem, 'gb');

            return $usedMemGb;
        }
    }

    /**
     * Get real free memory
     *
     * @param string $args
     *
     * @return float
     */
    public function getRealFreeMem($args)
    {
        $totalmem = $this->getTotalMem('kb');
        $usedmem = $this->getUsedMem('kb');

        $realfreemem = $totalmem - $usedmem;

        if ('kb' == $args) {
            return (int) $realfreemem;
        } elseif ('mb' == $args) {
            $realFreeMemMb = $this->convert($realfreemem, 'mb');

            return $realFreeMemMb;
        } elseif ('gb' == $args) {
            $realFreeMemGb = $this->convert($realfreemem, 'gb');

            return $realFreeMemGb;
        }
    }

    /**
     * Get used memory in purcentage
     *
     * @return float
     */
    public function getUsedMemPercentage()
    {
        $getused = $this->getUsedMem('kb') / $this->getTotalMem('kb');
        $sum = $getused * 100;
        $sum2 = $this->roundUp($sum, 2);

        return $sum2;
    }

    /**
     * Round up
     *
     * @param int    $value
     * @param string $precision
     *
     * @return int
     */
    public function roundUp($value, $precision)
    {
        $pow = pow(10, $precision);

        return (ceil($pow * $value) + ceil($pow * $value - ceil($pow * $value))) / $pow;
    }

    /**
     * Convert
     *
     * @param int    $input
     * @param string $conversion
     *
     * @return string
     */
    public function convert($input, $conversion)
    {
        if ('mb' == $conversion) {
            $converted = $input / 1024;

            return $this->roundUp($converted, 2);
        } elseif ('gb' == $conversion) {
            $converted = $input / 1024 / 1024;

            return $this->roundUp($converted, 2);
        }
    }
}
