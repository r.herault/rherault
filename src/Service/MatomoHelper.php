<?php

namespace App\Service;

use Symfony\Contracts\Cache\CacheInterface;
use VisualAppeal\Matomo;

class MatomoHelper
{

    private $matomoWrapper;
    private $cache;

    public function __construct(CacheInterface $cache)
    {
        $this->matomoWrapper = (new Matomo('https://analytics.rherault.fr', '68474290f3298b9e2de3e846b9c3334a', 2))
            ->setLanguage('fr')
            ->setPeriod(Matomo::PERIOD_WEEK)
            ->setDate(Matomo::DATE_TODAY);

        $this->cache = $cache;
    }

    public function getUniqueVisitors(): string
    {
        $visitors = $this->matomoWrapper->getUniqueVisitors();

        return $this->cache->get('matomo_visitors_'.md5($visitors), function () use ($visitors) {
            return $visitors;
        });
    }

    public function getVisitsTime(): string
    {
        $visitsTime = $this->matomoWrapper->getSumVisitsLengthPretty();

        return $this->cache->get('matomo_visits_time_'.md5($visitsTime), function () use ($visitsTime) {
            return $visitsTime;
        });
    }
}
