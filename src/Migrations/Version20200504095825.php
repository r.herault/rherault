<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200504095825 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE projet RENAME COLUMN titre TO title');
        $this->addSql('DROP INDEX uniq_497dd6346c6e55b5');
        $this->addSql('ALTER TABLE categorie RENAME COLUMN nom TO name');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_497DD6345E237E06 ON categorie (name)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP INDEX UNIQ_497DD6345E237E06');
        $this->addSql('ALTER TABLE categorie RENAME COLUMN name TO nom');
        $this->addSql('CREATE UNIQUE INDEX uniq_497dd6346c6e55b5 ON categorie (nom)');
        $this->addSql('ALTER TABLE projet RENAME COLUMN title TO titre');
    }
}
